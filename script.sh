export APP_PWD=password
db() {
    docker run -P --volumes-from app_data  -e POSTGRES_USER=app_user -e POSTGRES_PASSWORD=$APP_PWD --name app_db -d -t postgres:latest
}

app() {
    docker run -p 3000:3000 --link app_db:postgres --name app psagan/app
}

action=$1

${action}